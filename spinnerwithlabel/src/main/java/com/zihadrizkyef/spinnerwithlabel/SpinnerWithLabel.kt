package com.zihadrizkyef.spinnerwithlabel

import android.animation.ValueAnimator
import android.app.AlertDialog
import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.widget.ListPopupWindow
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.spinner_with_label.view.*


/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 27/02/19.
 */
class SpinnerWithLabel @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    enum class Mode(val id: Int) {
        DROPDOWN(0), DIALOG(1);

        companion object {
            fun fromId(id: Int): Mode {
                for (mode in values()) {
                    if (mode.id == id) return mode
                }
                throw IllegalArgumentException()
            }
        }
    }

    interface OnItemSelectedListener {
        fun onNothingSelected()
        fun onItemSelected(position: Int, item: String)
    }

    var floatingLabel = ""
        set(value) {
            field = value
            textLabel.text = value
        }

    var bigLabel = ""
        set(value) {
            field = value
            if (!isItemSelected()) {
                textSpinner.hint = value
            }
        }

    var itemArray = mutableListOf<String>()
        set(value) {
            field = value
            adapter.clear()
            adapter.addAll(value)
        }

    var spinnerMode = Mode.DROPDOWN
    var selectedPosition = -1
        private set
    var selectedItem = ""
        private set
    var isAnimateFloatLabel = true
    var isShowFloatLabel = true
        private set

    var error: String? = null
        private set

    var itemSelectedListeners = mutableListOf<OnItemSelectedListener>()

    var adapter = ArrayAdapter<String>(context, R.layout.spinner_dropdown, R.id.tvText)
        set(value) {
            field = value
            initDropdown()
            initDialog()
        }
    var errorColor = ContextCompat.getColor(context, R.color.color_error)
        set(value) {
            field = value
            textError.setTextColor(value)
        }
    private lateinit var defaultTextColor: ColorStateList
    private lateinit var dialog: AlertDialog
    private lateinit var dropDown: ListPopupWindow

    init {
        inflate(context, R.layout.spinner_with_label, this)

        initAttributes(attrs)
        initDropdown()
        initDialog()

        defaultTextColor = textLabel.textColors
        setOnClickListener {
            if (!adapter.isEmpty) {
                if (spinnerMode == Mode.DROPDOWN) {
                    dropDown.show()
                } else {
                    dialog.show()
                }
            }
        }
    }

    private fun initAttributes(attrs: AttributeSet? = null) {
        val heightAttr = attrs?.getAttributeValue(
            "http://schemas.android.com/apk/res/android",
            "layout_height"
        )
        setupConstraintByHeightAttr(heightAttr)

        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.SpinnerWithLabel, 0, 0)
        floatingLabel = a.getString(R.styleable.SpinnerWithLabel_floatingLabel) ?: ""
        bigLabel = a.getString(R.styleable.SpinnerWithLabel_bigLabel) ?: ""
        spinnerMode =
            Mode.fromId(a.getInt(R.styleable.SpinnerWithLabel_spinnerMode, Mode.DROPDOWN.id))
        isAnimateFloatLabel = a.getBoolean(R.styleable.SpinnerWithLabel_isAnimateFloatLabel, true)
        isShowFloatLabel = a.getBoolean(R.styleable.SpinnerWithLabel_isShowFloatLabel, true)
        if (a.getBoolean(R.styleable.SpinnerWithLabel_showBottomLine, true)) {
            line.visibility = VISIBLE
        } else {
            line.visibility = INVISIBLE
        }
        a.recycle()
    }

    private fun setupConstraintByHeightAttr(heightAttr: String?) {
        if (heightAttr != ViewGroup.LayoutParams.WRAP_CONTENT.toString()) {
            (textError.layoutParams as LayoutParams).apply {
                topToBottom = LayoutParams.UNSET
                bottomToBottom = id
            }
            (line.layoutParams as LayoutParams).apply {
                topToBottom = LayoutParams.UNSET
                bottomToTop = textError.id
            }
            (textSpinner.layoutParams as LayoutParams).apply {
                bottomToTop = line.id
                height = LayoutParams.MATCH_CONSTRAINT
            }
            requestLayout()
        }
    }

    private fun initDropdown() {
        dropDown = ListPopupWindow(context)
        dropDown.anchorView = this
        dropDown.setAdapter(adapter)
        dropDown.setOnItemClickListener { _, _, position, _ ->
            val shouldAnimate = !isItemSelected()
            selectedPosition = position
            selectedItem = adapter.getItem(position)!!
            textSpinner.text = selectedItem
            if (isShowFloatLabel) {
                if (isAnimateFloatLabel && shouldAnimate) {
                    animateShowFloatLabel()
                } else {
                    if (textLabel.layoutParams.height != LayoutParams.WRAP_CONTENT) {
                        textLabel.layoutParams.height = LayoutParams.WRAP_CONTENT
                        textLabel.requestLayout()
                    }
                    textLabel.visibility = VISIBLE
                }
            }

            itemSelectedListeners.forEach { it.onItemSelected(position, selectedItem) }
            dropDown.dismiss()
        }

        dropDown.setOnDismissListener {
            itemSelectedListeners.forEach { it.onNothingSelected() }
        }
    }

    private fun initDialog() {
        dialog = AlertDialog.Builder(context)
            .setAdapter(adapter) { _, position ->
                val shouldAnimate = !isItemSelected()
                selectedPosition = position
                selectedItem = adapter.getItem(position)!!
                textSpinner.text = selectedItem
                if (isShowFloatLabel) {
                    if (isAnimateFloatLabel && shouldAnimate) {
                        animateShowFloatLabel()
                    } else {
                        if (textLabel.layoutParams.height != LayoutParams.WRAP_CONTENT) {
                            textLabel.layoutParams.height = LayoutParams.WRAP_CONTENT
                            textLabel.requestLayout()
                        }
                        textLabel.visibility = VISIBLE
                    }
                }

                itemSelectedListeners.forEach { it.onItemSelected(position, selectedItem) }
            }.create()

        dialog.setOnDismissListener {
            itemSelectedListeners.forEach { it.onNothingSelected() }
        }
    }

    private fun animateShowFloatLabel() {
        textLabel.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        val actualHeight = textLabel.measuredHeight
        textLabel.layoutParams.height = 0
        textLabel.requestLayout()
        textLabel.visibility = View.VISIBLE

        val anim = ValueAnimator.ofInt(0, actualHeight)
        anim.interpolator = DecelerateInterpolator()
        anim.addUpdateListener { valueAnimator ->
            val value = valueAnimator.animatedValue as Int
            textLabel.layoutParams.height = value
            textLabel.requestLayout()
        }
        anim.duration = 300
        anim.start()
    }

    private fun animateHideFloatLabel() {
        textLabel.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        val actualHeight = textLabel.measuredHeight

        val anim = ValueAnimator.ofInt(actualHeight, 0)
        anim.interpolator = DecelerateInterpolator()
        anim.addUpdateListener { valueAnimator ->
            val value = valueAnimator.animatedValue as Int
            textLabel.layoutParams.height = value
            textLabel.requestLayout()

            if (value == 0) {
                textLabel.visibility = View.GONE
            }
        }
        anim.duration = 300
        anim.start()
    }

    private fun animateShowError() {
        textError.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        val actualHeight = textError.measuredHeight
        textError.layoutParams.height = 0
        textError.requestLayout()
        textError.visibility = View.VISIBLE

        val anim = ValueAnimator.ofInt(0, actualHeight)
        anim.interpolator = DecelerateInterpolator()
        anim.addUpdateListener { valueAnimator ->
            val value = valueAnimator.animatedValue as Int
            textError.layoutParams.height = value
            textError.requestLayout()
        }
        anim.duration = 300
        anim.start()
    }

    private fun animateHideError() {
        textError.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        val actualHeight = textError.measuredHeight

        val anim = ValueAnimator.ofInt(actualHeight, 0)
        anim.interpolator = DecelerateInterpolator()
        anim.addUpdateListener { valueAnimator ->
            val value = valueAnimator.animatedValue as Int
            textError.layoutParams.height = value
            textError.requestLayout()

            if (value == 0) {
                textError.visibility = View.GONE
            }
        }
        anim.duration = 300
        anim.start()
    }

    fun setShowFloatLabel(isShow: Boolean) {
        isShowFloatLabel = isShow
        if (isShow) {
            if (isItemSelected()) {
                animateShowFloatLabel()
            }
        }
    }

    fun setError(@StringRes errorRes: Int, animate: Boolean = true) {
        setError(context.getString(errorRes), animate)
    }

    fun setError(error: String?, animate: Boolean = true) {
        if (error != null) {
            textError.text = error
            textLabel.setTextColor(errorColor)
            line.setBackgroundColor(errorColor)
            if (animate) {
                animateShowError()
            } else {
                if (textError.layoutParams.height != LayoutParams.WRAP_CONTENT) {
                    textError.layoutParams.height = LayoutParams.WRAP_CONTENT
                    textError.requestLayout()
                }
                textError.visibility = View.VISIBLE
            }
        } else {
            textLabel.setTextColor(defaultTextColor)
            line.setBackgroundColor(ContextCompat.getColor(context, R.color.line_color))
            if (animate) animateHideError() else textError.visibility = View.GONE
        }
    }

    fun isItemSelected(): Boolean {
        return selectedPosition != -1
    }

    fun setSelectedPosition(pos: Int, animate: Boolean = true) {
        if (pos == -1) {
            selectedPosition = -1
            selectedItem = ""
            textSpinner.text = ""
            if (isAnimateFloatLabel && animate) {
                animateHideFloatLabel()
            } else {
                textLabel.visibility = View.GONE
            }
        } else {
            val isAlreadySelected = isItemSelected()
            selectedPosition = pos
            selectedItem = itemArray[pos]
            textSpinner.text = selectedItem
            if (isShowFloatLabel) {
                if (isAnimateFloatLabel && !isAlreadySelected && animate) {
                    animateShowFloatLabel()
                } else {
                    if (textLabel.layoutParams.height != LayoutParams.WRAP_CONTENT) {
                        textLabel.layoutParams.height = LayoutParams.WRAP_CONTENT
                        textLabel.requestLayout()
                    }
                    textLabel.visibility = VISIBLE
                }
            }
        }
    }

    fun getFloatingTextView(): TextView = textLabel

    fun getTextView(): TextView = textSpinner

    fun getLineBottom(): View = line

    fun getArrowImage(): ImageView = imageDropdown
}