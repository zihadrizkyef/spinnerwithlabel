package com.zihadrizkyef.spinnerwithlabel

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        spinnerName.itemArray = arrayListOf(
            "Lewis George",
            "Callum Black",
            "Aaron Rose",
            "Morgan Rose",
            "Tommy Burns",
            "Alan Fulton",
            "Evan Burnett",
            "Kendrick Newton",
            "Marco Carson",
            "Atticus Sanchez"
        )
        spinnerName.itemSelectedListeners.add(object : SpinnerWithLabel.OnItemSelectedListener {
            override fun onNothingSelected() {}
            override fun onItemSelected(position: Int, item: String) {
                showSnackbar(item)
            }
        })

        spinnerName.itemSelectedListeners.add(object : SpinnerWithLabel.OnItemSelectedListener {
            override fun onNothingSelected() {}
            override fun onItemSelected(position: Int, item: String) {
                Toast.makeText(this@MainActivity, item, Toast.LENGTH_LONG).show()
                spinnerName.itemSelectedListeners.remove(this)
            }
        })

        spinnerAge.itemArray = (15..40).map { it.toString() }.toMutableList()
        spinnerAge.itemSelectedListeners.add(object : SpinnerWithLabel.OnItemSelectedListener {
            override fun onNothingSelected() {}
            override fun onItemSelected(position: Int, item: String) {
                showSnackbar(item)
            }
        })

        spinnerColor.itemArray = mutableListOf(
            "Red",
            "Orange",
            "Yellow",
            "Chartreuse green",
            "Green",
            "Spring green",
            "Cyan",
            "Azure",
            "Blue",
            "Violet",
            "Magenta",
            "Rose"
        )
        spinnerColor.itemSelectedListeners.add(object : SpinnerWithLabel.OnItemSelectedListener {
            override fun onNothingSelected() {}
            override fun onItemSelected(position: Int, item: String) {
                showSnackbar(item)
            }
        })
    }

    private fun showSnackbar(text: String) {
        Snackbar.make(rootLayout, text, Snackbar.LENGTH_LONG)
            .setAnimationMode(Snackbar.ANIMATION_MODE_FADE)
            .show()
    }
}
