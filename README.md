# SPINNER WITH LABEL
Giving spinner a label/hint which the hint should not appear in items dropdown

## HOW TO USE:

Add this to your XML
```
<com.zihadrizkyef.spinnerwithlabel.SpinnerWithLabel
    android:id="@+id/spinnerName"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginTop="8dp"
    android:background="#EEF8EC"
    app:bigLabel="Choose Name :"
    app:floatingLabel="Name" />
```

Add this to your MainActivity
```
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        spinnerName.itemArray = arrayListOf(
            "Lewis George",
            "Callum Black",
            "Aaron Rose",
            "Morgan Rose",
            "Tommy Burns",
            "Alan Fulton",
            "Evan Burnett",
            "Kendrick Newton",
            "Marco Carson",
            "Atticus Sanchez"
        )
        spinnerName.onItemSelectedListener = object : SpinnerWithLabel.OnItemSelectedListener {
            override fun onNothingSelected() {}
            override fun onItemSelected(position: Int, item: String) {
                showSnackbar(item)
            }
        }
    }
}
```

Here is the result :
![Sample Video](recording/video1.mp4)
